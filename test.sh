#!/usr/bin/env bash

set -ex

ARCH="$1"

case $ARCH in
  x86_64) mingw=clang64 ;;
  *) echo "Unknown architecture $ARCH"; exit 1 ;;
esac

rm -Rf tmp
mkdir -p tmp
cd tmp

for f in $(cat ../tarballs/$ARCH/MANIFEST | tr -d "\r"); do
  tar -xf ../tarballs/$ARCH/$f
done

cat >hello.c <<EOF
#include <stdio.h>
int main() {
  printf("Hello world!");
  return 0;
}
EOF

CC="`pwd`/$mingw/bin/clang.exe"
CFLAGS="-I`pwd`/$mingw/include -Werror"
export PATH="`pwd`/$mingw/bin:$PATH"
"$CC" --version
"$CC" $CFLAGS hello.c -o hello
./hello

"$CC" --print-search-dirs | grep -v -q '[ABDEFGH]:/' || \
    ( echo "found upstream packaging error: non-C: paths in clang search path (see ghc#18774)"; exit 1 )

cd ..
rm -Rf tmp
