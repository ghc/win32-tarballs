#!/usr/bin/env python3

from typing import List, Dict, NewType
import requests
from pathlib import Path
import tempfile
import subprocess
import hashlib
import shutil

Arch = NewType('Arch', str)
Url = str

UPSTREAM_URL = 'http://mirror.msys2.org/mingw'

CHECK_SIGNATURES = False
JAILBREAK = Path('ghc-jailbreak')

# Known architectures (upstream names)
ARCHS = ['x86_64', 'sources'] # type: List[Arch] valid entries x86_64, i686, sources

def package(name: str, archs: List[Arch] = ARCHS, ext: str = 'zst', src_ext: str = 'gz') -> Dict[Arch, Url]:
    # N.B. ext is to accomodate transition from .xz to .zst archives.
    URLS = {
        'x86_64':  f'/clang64/mingw-w64-clang-x86_64-{name}-any.pkg.tar.{ext}',
        'i686':  f'/clang32/mingw-w64-clang-i686-{name}-any.pkg.tar.{ext}',
        'sources':  f'/sources/mingw-w64-{name}.src.tar.{src_ext}',
    }
    return { arch: URLS[arch] for arch in archs }

# Version numbers
crt_ver = '9.0.0.6316.acdc7adc9'
winpthreads_ver = '9.0.0.6451.a3f6d363d-1'
gcc_ver = '10.2.0'
llvm_ver = '14.0.6-1'

# type: List[Dict[Arch, Url]]
packages = [
    package(f'crt-git-{crt_ver}-1'),
    package(f'headers-git-{crt_ver}-1'),
    package('windows-default-manifest-6.4-4'),

    # dependencies of Clang/LLVM
    package('libxml2-2.10.3-1', archs=['i686', 'x86_64'], src_ext='zst'),
    package('libffi-3.3-4', archs=['i686', 'x86_64']),
    package(f'libwinpthread-git-{winpthreads_ver}', archs=['i686', 'x86_64']),
    package(f'winpthreads-git-{winpthreads_ver}', archs=['i686', 'x86_64']),
    package('libiconv-1.17-1', archs=['i686', 'x86_64']),
    package('zlib-1.2.13-1', src_ext='zst'),
    package('xz-5.2.9-1', archs=['i686', 'x86_64']),
    package('zstd-1.5.2-2'),

    # LLVM
    package(f'clang-{llvm_ver}', src_ext='zst'),
    package(f'llvm-{llvm_ver}', archs=['i686', 'x86_64'], src_ext='zst'),
    package(f'lld-{llvm_ver}', archs=['i686', 'x86_64'], src_ext='zst'),
    package(f'compiler-rt-{llvm_ver}', archs=['i686', 'x86_64'], src_ext='zst'),
    package(f'libc++-{llvm_ver}', archs=['i686', 'x86_64'], src_ext='zst'),
    package(f'libc++abi-{llvm_ver}', archs=['i686', 'x86_64'], src_ext='zst'),
    package(f'libunwind-{llvm_ver}', archs=['i686', 'x86_64'], src_ext='zst'),
]

def download(url: str, dest: Path):
    print(f'Fetching {url} => {dest}...')
    r = requests.get(url, stream=True)
    r.raise_for_status()
    out = dest.open('wb')
    for chunk in r.iter_content(chunk_size=128):
        out.write(chunk)

def download_tarballs(dest_dir: Path) -> Dict[Arch, List[Path]]:
    tarballs = { arch: [] for arch in ARCHS }
    for pkg in packages:
        for arch in ARCHS:
            if arch in pkg:
                (dest_dir / arch).mkdir(parents=True, exist_ok=True)
                url = f'{UPSTREAM_URL}{pkg[arch]}'
                dest = dest_dir / arch / Path(pkg[arch]).name
                dest_sig = dest.with_name(dest.name + '.sig')
                if not dest.exists():
                    download(url, dest)

                if CHECK_SIGNATURES:
                    download(url + '.sig', dest_sig)
                    subprocess.check_call(['gpgv', '-v',  '--keyring', './mingw-key.gpg', dest_sig, dest])
                tarballs[arch].append(dest)

    return tarballs

def patch_executable(arch: Arch, exe: Path) -> None:
    patcher_dir = JAILBREAK / arch
    patcher = patcher_dir / 'iat-patcher.exe'
    print(f'Patching {exe} with {patcher}...')
    subprocess.check_call([patcher, 'install', exe])

    for dll in patcher_dir.glob('*.dll'):
        dest = exe.parent / dll.name
        if not dest.is_file():
            shutil.copyfile(dll, dest)

def needs_patching(tarball: Path) -> bool:
    return 'binutils' in tarball.name or 'gcc' in tarball.name

def patch_tarball(arch: Arch, tarball: Path) -> Path:
    if not needs_patching(tarball):
        return tarball

    dest = tarball.with_name(tarball.name.replace('-any', '-phyx'))
    if not dest.exists():
        print(f'Patching tarball {tarball} for {arch}...')
        # We always recompress to xz
        dest = dest.with_suffix('.xz')
        with tempfile.TemporaryDirectory() as tmp:
            subprocess.check_call(['tar', '-xf', tarball, '-C', tmp])
            for f in Path(tmp).glob('**/*.exe'):
                patch_executable(arch, f)

            subprocess.check_call(['tar', '-cJf', dest, '-C', tmp, '.'])

    return dest

def hash_file(path: Path) -> Path:
    h = hashlib.new('sha256')
    h.update(path.read_bytes())
    return h.hexdigest()

def main() -> None:
    dest = Path('tarballs')
    tarballs = download_tarballs(dest)

    for arch, files in tarballs.items():
        d = dest / arch
        patched_files = []
        for f in files:
            tarball = patch_tarball(arch, f)
            hash = hash_file(tarball)
            patched_files.append(tarball.relative_to(d))

        (d / "MANIFEST").write_text('\n'.join(str(f) for f in patched_files))
        subprocess.run(['sha256sum'] + [f.relative_to(d) for f in d.iterdir()],
                       stdout=open(d / 'SHA256SUMS', 'wb'),
                       cwd=d,
                       check=True)

    # Preserve commit hash
    commit = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    (dest / "COMMIT").write_bytes(commit)

    shutil.copyfile('ghc-jailbreak.tar.gz', dest / 'ghc-jailbreak.tar.gz')

if __name__ == '__main__':
    main()
